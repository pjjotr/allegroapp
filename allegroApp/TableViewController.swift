//
//  TableViewController.swift
//  allegroApp
//
//  Created by Piotr Łopata on 29.04.2016.
//  Copyright © 2016 Piotr Łopata. All rights reserved.
//

import UIKit
import Kanna

class TableViewController: UITableViewController {

    let html = "http://allegro.pl/listing/listing.php?order=m&string=iphone+6&bmatch=engagement-v6-promo-sm-sqm-dyn-fall-v2-ele-1-3-0205"
    
    var phones: NSMutableArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getHTML()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.phones.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)

        let phone = self.phones.objectAtIndex(indexPath.row) as! IPhone
        
        cell.textLabel?.text = phone.title
        cell.detailTextLabel?.text = phone.price

        return cell
    }

    
    // MARK: - parse HTML
    
    
    func getHTML() {
        do {
            let HTMLString = try String(contentsOfURL: NSURL(string:self.html)!)
            
            parser(HTMLString)
        } catch let error as NSError {
            print("Error: \(error)")
        }
    }
    
    func parser(htmlString: String) {
        if let doc = Kanna.HTML(html: htmlString, encoding: NSUTF8StringEncoding) {
            
            // searching important data
            
            for link in doc.xpath("//a[@class=\"offer-title\"] | //span[starts-with(@class, \"offer-buy-now\")]/span[@class=\"statement\"]/text()") {
                
                // much easier way to create objects - JSON
                
                let newText = link.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                
                if newText!.hasSuffix("zł") {
                    let phone = self.phones.lastObject as! IPhone
                    
                    phone.price = newText!
                }
                else if (newText != ""){
                    let newPhone = IPhone(title: newText!)
                    self.phones.addObject(newPhone)
                }
                
            }
            
        }
        
        tableView.reloadData()
        
    }
}
